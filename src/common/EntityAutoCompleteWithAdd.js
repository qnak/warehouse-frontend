import {AutoComplete} from "primereact/autocomplete";
import {Button} from "primereact/button";
import {Dialog} from "primereact/dialog";
import {InputText} from "primereact/inputtext";
import {classNames} from "primereact/utils";
import React, {useState} from 'react';

export function EntityAutoCompleteWithAdd(props) {
    const emptyEntity = {name: null, code: null};

    const [selectedEntity, setSelectedEntity] = useState(props.entity ? {...props.entity} : null);
    const [filteredEntity, setFilteredEntity] = useState([]);
    const [entity, setEntity] = useState(emptyEntity);
    const [addNewDialogVisible, setAddNewDialogVisible] = useState(false);
    const [submitted, setSubmitted] = useState(false);

    const searchEntity = (event) => {
        props.findByQuery(event.query).then(result => {
            setFilteredEntity(result.data);
        })
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || null;
        let _entity = {...entity};
        _entity[`${name}`] = val;

        setEntity(_entity);
    }

    const openNew = () => {
        setEntity(emptyEntity);
        setSubmitted(false);
        setAddNewDialogVisible(true);
    }

    const saveEntity = () => {
        setSubmitted(true);
        if (entity.name && entity.code) {
            props.createEntity(entity).then(result => {
                hideDialog();
            });
        }
    }

    const hideDialog = () => {
        setSubmitted(false);
        setAddNewDialogVisible(false);
    }

    const dialogFooter = (
        <React.Fragment>
            <Button label="Закрыть" icon="pi pi-times" className="p-button-text" onClick={hideDialog}/>
            <Button label="Сохранить" icon="pi pi-check" className="p-button-text" onClick={saveEntity}/>
        </React.Fragment>
    );

    return (
        <div className="inline-flex card-container">
            <div className={'inline'}>
                <AutoComplete dropdown
                              value={selectedEntity}
                              field={"name"}
                              suggestions={filteredEntity}
                              completeMethod={searchEntity}
                              onChange={(e) => {
                                  setSelectedEntity(e.value);
                                  props.onEntityChanged(e.value);
                              }}/>
            </div>

            <div className={'inline'}>
                <Button icon={"pi pi-plus-circle"}
                        style={{display: props.createEntity ? 'inline-block' : 'none'}}
                        title={"Добавить"}
                        onClick={openNew}/>
            </div>

            <Dialog visible={addNewDialogVisible}
                    style={{width: '450px'}}
                    header="Добавление"
                    modal
                    className="p-fluid"
                    footer={dialogFooter}
                    onHide={hideDialog}>

                <div className="field">
                    <label htmlFor="name">Название</label>
                    <InputText id="name"
                               value={entity.name}
                               onChange={(e) => onInputChange(e, 'name')}
                               required
                               autoFocus className={classNames({'p-invalid': submitted && !entity.name})}/>
                    {submitted && !entity.name && <small className="p-error">Название не заполнено</small>}
                </div>
                <div className="field">
                    <label className="mb-3">Код</label>
                    <InputText id="name"
                               value={entity.code}
                               onChange={(e) => onInputChange(e, 'code')}
                               required
                               className={classNames({'p-invalid': submitted && !entity.code})}/>
                    {submitted && !entity.code && <small className="p-error">Код не заполнен</small>}
                </div>
            </Dialog>
        </div>
    )
}