import {backendApi} from "../../common/backendApi";

export class ProductService {
    getProducts(pageConfig) {
        return backendApi.get(`/products?page=${pageConfig.page}&size=${pageConfig.size}&sort=id`);
    }

    getProductsByName(query) {
        return backendApi.get(`/products/filter?name=${query}`);
    }

    createProduct(product) {
        return backendApi.post(`/products`, product);
    }

    updateProductDescription(product) {
        return backendApi.patch(`/products/${product.id}?description=${product.description}`);
    }
}

export const productService = new ProductService();