import React, {useEffect, useRef, useState} from 'react';
import {classNames} from 'primereact/utils';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {InputTextarea} from 'primereact/inputtextarea';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {useDispatch, useSelector} from "react-redux";
import {loadProducts, selectProductPage, selectProducts, selectProductsStatus} from "./ProductSlice";
import {productService} from "./ProductService";

export function Products() {

    let emptyProduct = {
        id: null,
        name: '',
        description: '',
        brand: ''
    };
    const productsPage = useSelector(selectProductPage);
    const products = useSelector(selectProducts);
    const loadingStatus = useSelector(selectProductsStatus);

    const dispatch = useDispatch();

    const [productDialogVisible, setProductDialogVisible] = useState(false);
    const [isProductUpdate, setIsProductUpdate] = useState(false);
    const [product, setProduct] = useState(emptyProduct);
    const [submitted, setSubmitted] = useState(false);

    const toast = useRef(null);
    const dt = useRef(null);

    function reloadProducts() {
        dispatch(loadProducts({
            size: productsPage.pageable.pageSize,
            page: productsPage.pageable.offset
        }))
    }

    useEffect(() => {
        reloadProducts();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps


    const openNew = () => {
        setProduct(emptyProduct);
        setSubmitted(false);
        setProductDialogVisible(true);
        setIsProductUpdate(false);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialogVisible(false);
    }

    const saveProduct = () => {
        setSubmitted(true);
        if (!product.name || !product.brand) {
            return;
        }
        if (isProductUpdate) {
            productService.updateProductDescription(product).then(result => {
                setProductDialogVisible(false);
                toast.current.show({severity: 'success', summary: 'Продукт обновлен'});
                reloadProducts();

            });
        } else {
            productService.createProduct(product).then(result => {
                setProductDialogVisible(false);
                toast.current.show({severity: 'success', summary: 'Продукт добавлен'});
                dispatch(loadProducts({size: productsPage.pageable.pageSize, page: productsPage.pageable.offset}));
                reloadProducts();
            });
        }
    }

    const editProduct = (product) => {
        setProduct({...product});
        setProductDialogVisible(true);
        setIsProductUpdate(true);
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || null;
        let _product = {...product};
        _product[`${name}`] = val;

        setProduct(_product);
    }

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Button label="Добавить" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew}/>
            </React.Fragment>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2"
                        onClick={() => editProduct(rowData)}/>
            </React.Fragment>
        );
    }

    const productDialogFooter = (
        <React.Fragment>
            <Button label="Закрыть" icon="pi pi-times" className="p-button-text" onClick={hideDialog}/>
            <Button label="Сохранить" icon="pi pi-check" className="p-button-text" onClick={saveProduct}/>
        </React.Fragment>
    );

    return (
        <div className="datatable-crud-demo">
            <Toast ref={toast}/>

            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate}></Toolbar>

                <DataTable ref={dt}
                           value={products}
                           dataKey="id"
                           lazy
                           paginator
                           onPage={(e) => dispatch(loadProducts({page: e.page, size: e.rows}))}
                           first={productsPage.pageable.offset}
                           rows={productsPage.pageable.pageSize}
                           loading={loadingStatus === 'loading'}
                           rowsPerPageOptions={[5, 10, 25]}
                           totalRecords={productsPage.totalElements}
                           paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                           currentPageReportTemplate="с {first} по {last} из {totalRecords} продуктов"
                           responsiveLayout="scroll">

                    <Column field="id" header="Код Продукта" style={{minWidth: '10rem'}}></Column>
                    <Column field="name" header="Название" style={{minWidth: '16rem'}}></Column>
                    <Column field="description" header="Описание" style={{minWidth: '16rem'}}></Column>
                    <Column field="brand" header="Бренд" style={{minWidth: '12rem'}}></Column>

                    <Column body={actionBodyTemplate} exportable={false} style={{minWidth: '5rem'}}></Column>
                </DataTable>
            </div>

            <Dialog visible={productDialogVisible}
                    style={{width: '450px'}}
                    header="Продукт" modal className="p-fluid"
                    footer={productDialogFooter}
                    onHide={hideDialog}>

                <div className="field">
                    <label>Название</label>
                    <InputText value={product.name} onChange={(e) => onInputChange(e, 'name')} required
                               disabled={isProductUpdate}
                               autoFocus className={classNames({'p-invalid': submitted && !product.name})}/>
                    {submitted && !product.name && <small className="p-error">Название не заполнено</small>}
                </div>

                <div className="field">
                    <label>Описание</label>
                    <InputTextarea value={product.description}
                                   autoFocus={isProductUpdate}
                                   onChange={(e) => onInputChange(e, 'description')} required rows={3} cols={20}/>
                </div>

                <div className="field">
                    <label className="mb-3">Бренд</label>
                    <InputText value={product.brand} disabled={isProductUpdate}
                               onChange={(e) => onInputChange(e, 'brand')} required
                               className={classNames({'p-invalid': submitted && !product.brand})}/>
                    {submitted && !product.brand && <small className="p-error">Бренд не заполнен</small>}
                </div>
            </Dialog>
        </div>
    );
}
                 