import {backendApi} from "../../common/backendApi";
import {formatDate} from "../../common/utils";

export class SupplyOrderService {
    getSupplyOrders(loadingConfig) {
        return backendApi.get(`/supply-orders?page=${loadingConfig.page}&size=${loadingConfig.size}&sort=deliveredOn,desc`);
    }

    createSupplyOrder(supplyOrder) {
        return backendApi.post(`/supply-orders`, {
            productId: supplyOrder.product.id,
            supplierId: supplyOrder.supplier.id,
            quantity: supplyOrder.quantity,
            deliveredOn: formatDate(supplyOrder.deliveredOn)
        });
    }
}

export const supplyOrderService = new SupplyOrderService();