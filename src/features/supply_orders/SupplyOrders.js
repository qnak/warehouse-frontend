import React, {useEffect, useRef, useState} from 'react';
import {classNames} from 'primereact/utils';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toast} from 'primereact/toast';
import {Button} from 'primereact/button';
import {Toolbar} from 'primereact/toolbar';
import {Dialog} from 'primereact/dialog';
import {EntityAutoCompleteWithAdd} from "../../common/EntityAutoCompleteWithAdd";
import {productService} from "../products/ProductService";
import {Suppliers} from "../suppliers/Suppliers";
import {InputNumber} from "primereact/inputnumber";
import {supplyOrderService} from "./SupplyOrderService";
import {Calendar} from "primereact/calendar";

export function SupplyOrders() {

    const emptySupplyOrder = {
        id: null,
        quantity: 1,
        deliveredOn: new Date(),
        product: null,
        supplier: null
    };

    const pageInitialState = {
        offset: 0,
        pageSize: 5
    };
    const minDate = new Date();
    const [page, setPage] = useState(pageInitialState);

    const [supplyOrders, setSupplyOrders] = useState([]);
    const [loadingStatus, setLoadingStatus] = useState(false);

    const [supplyOrderDialogVisible, setSupplyOrderDialogVisible] = useState(false);
    const [supplyOrder, setSupplyOrder] = useState(emptySupplyOrder);
    const [submitted, setSubmitted] = useState(false);

    const toast = useRef(null);

    function reloadSupplyOrders(loadingConfig) {
        supplyOrderService.getSupplyOrders(loadingConfig)
            .then(result => {
                setPage({
                    ...result.data.pageable,
                    totalElements: result.data.totalElements
                });
                setLoadingStatus(false);
                setSupplyOrders(result.data.content);
            });
    }

    useEffect(() => {
        reloadSupplyOrders({page: 0, size: 5});
    }, []); // eslint-disable-line react-hooks/exhaustive-deps


    const openNew = () => {
        setSupplyOrder(emptySupplyOrder);
        setSubmitted(false);
        setSupplyOrderDialogVisible(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setSupplyOrderDialogVisible(false);
    }

    const saveProduct = () => {
        setSubmitted(true);
        supplyOrderService.createSupplyOrder(supplyOrder)
            .then(result => {
                setSupplyOrderDialogVisible(false);
                toast.current.show({severity: 'success', summary: 'Поставка создана'});
                reloadSupplyOrders({page: 0, size: 5});
            })
            .catch(error => {
                toast.current.show({
                    severity: 'error',
                    summary: 'Поставка не создана',
                    detail: 'Что-то пошло не так :('
                });
                console.log(`Error while creating ${error}`);
            });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || null;
        let _product = {...supplyOrder};
        _product[`${name}`] = val;
        setSupplyOrder(_product);
    }

    const onInputNumberChange = (e, name) => {
        const val = (e.value) || 0;
        let _supOrder = {...supplyOrder};
        _supOrder[`${name}`] = val;
        setSupplyOrder(_supOrder);
    }

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Button label="Добавить" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew}/>
            </React.Fragment>
        )
    }

    const supplyOrderDialogFooter = (
        <React.Fragment>
            <Button label="Закрыть" icon="pi pi-times" className="p-button-text" onClick={hideDialog}/>
            <Button label="Сохранить" icon="pi pi-check" className="p-button-text" onClick={saveProduct}/>
        </React.Fragment>
    );

    let searchProductByName = (name) => {
        return productService.getProductsByName(name);
    }

    const supplierChanged = (newSupplier) => {
        setSupplyOrder({
            ...supplyOrder,
            supplier: newSupplier
        })
    };

    const productChanged = (newProduct) => setSupplyOrder({
        ...supplyOrder,
        product: newProduct
    });

    return (
        <div className="datatable-crud-demo">
            <Toast ref={toast}/>

            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate}></Toolbar>

                <DataTable value={supplyOrders}
                           dataKey="id"
                           lazy
                           paginator
                           onPage={(e) => reloadSupplyOrders({page: e.page, size: e.rows})}
                           first={page.offset}
                           rows={page.pageSize}
                           loading={loadingStatus}
                           rowsPerPageOptions={[5, 10, 25]}
                           totalRecords={page.totalElements}
                           paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                           currentPageReportTemplate="с {first} по {last} из {totalRecords} заказов"
                           responsiveLayout="scroll">
                    <Column field="id" header="Код Поставки" style={{minWidth: '5rem'}}></Column>
                    <Column field="product.name" header="Продукт" style={{minWidth: '16rem'}}></Column>
                    <Column field="supplier.name" header="Поставщик" style={{minWidth: '16rem'}}></Column>
                    <Column field="quantity" header="Количество" style={{minWidth: '5rem'}}></Column>
                    <Column field="deliveredOn" header="Дата Поставки" style={{minWidth: '8rem'}}></Column>
                </DataTable>
            </div>
            {/*Create new supply order dialog*/}
            <Dialog visible={supplyOrderDialogVisible}
                    style={{width: '550px'}}
                    header="Добавление новой поставки"
                    modal
                    className="p-fluid"
                    footer={supplyOrderDialogFooter}
                    onHide={hideDialog}>

                <div className="field">
                    <label>Продукт</label>
                    <div>
                        <EntityAutoCompleteWithAdd
                            findByQuery={searchProductByName}
                            onEntityChanged={productChanged}/>
                    </div>
                </div>
                <div className="field">
                    <label>Поставщик</label>
                    <div>
                        <Suppliers onSupplierChanged={supplierChanged}/>
                    </div>
                </div>
                <div className="field">
                    <label>Количество</label>
                    <InputNumber id="quantity"
                                 value={supplyOrder.quantity}
                                 onChange={(e) => onInputNumberChange(e, 'quantity')}
                                 required
                                 className={classNames({'p-invalid': submitted && !supplyOrder.quantity})}/>
                </div>

                <div className="field">
                    <label>Дата поставки</label>
                    <Calendar id="deliveredOn"
                              dateFormat={"yy-mm-dd"}
                              value={supplyOrder.deliveredOn}
                              minDate={minDate}
                              onChange={(e) => onInputChange(e, 'deliveredOn')}
                              required
                              className={classNames({'p-invalid': submitted && !supplyOrder.deliveredOn})}/>
                </div>
            </Dialog>
        </div>
    );
}
                 