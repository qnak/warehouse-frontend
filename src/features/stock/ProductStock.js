import React, {useEffect, useState} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Toolbar} from 'primereact/toolbar';
import {productStockService} from "./ProductStockService";
import {Calendar} from "primereact/calendar";

export function ProductStock() {

    const pageInitialState = {
        offset: 0,
        pageSize: 5
    };

    const [page, setPage] = useState(pageInitialState);
    const [stockDate, setStockDate] = useState(new Date());
    const [supplyOrders, setSupplyOrders] = useState([]);
    const [loadingStatus, setLoadingStatus] = useState(false);


    const reloadSupplyOrders = (loadingConfig) => {
        productStockService.getProductStock(loadingConfig)
            .then(result => {
                setPage({
                    ...result.data.pageable,
                    totalElements: result.data.totalElements
                });
                setLoadingStatus(false);
                setSupplyOrders(result.data.content);
            });
    }

    const getDefaultPageConfigOnDate = (date) => {
        return {page: 0, size: 5, date: date};
    }

    useEffect(() => {
        reloadSupplyOrders(getDefaultPageConfigOnDate(stockDate));
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Calendar value={stockDate}
                          onChange={(e) => {
                              setStockDate(e.value);
                              reloadSupplyOrders(getDefaultPageConfigOnDate(e.value))
                          }}
                          dateFormat={"yy-mm-dd"}
                />
            </React.Fragment>
        )
    }

    return (
        <div>
            <div className="card">
                <Toolbar className="mb-4" left={leftToolbarTemplate}></Toolbar>

                <DataTable value={supplyOrders}
                           dataKey="id"
                           lazy
                           paginator
                           onPage={(e) => reloadSupplyOrders({page: e.page, size: e.rows, date: stockDate})}
                           first={page.offset}
                           rows={page.pageSize}
                           loading={loadingStatus}
                           rowsPerPageOptions={[5, 10, 25]}
                           totalRecords={page.totalElements}
                           paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                           currentPageReportTemplate="c {first} по {last} из {totalRecords} записей"
                           responsiveLayout="scroll">

                    <Column field="productId" header="Код Продукта" style={{minWidth: '5rem'}}></Column>
                    <Column field="name" header="Название" style={{minWidth: '16rem'}}></Column>
                    <Column field="stock" header="Запас" style={{minWidth: '16rem'}}></Column>

                </DataTable>
            </div>
        </div>
    );
}
                 