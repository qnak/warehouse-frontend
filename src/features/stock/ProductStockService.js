import {backendApi} from "../../common/backendApi";
import {formatDate} from "../../common/utils";


export class ProductStockService {
    getProductStock(loadingConfig) {
        return backendApi.get(`/products-stock?date=${formatDate(loadingConfig.date)}&page=${loadingConfig.page}&size=${loadingConfig.size}&sort=stock,asc`);
    }
}

export const productStockService = new ProductStockService();