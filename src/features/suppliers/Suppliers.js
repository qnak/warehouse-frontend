import {supplierService} from "./SupplierService";
import React from 'react';
import {EntityAutoCompleteWithAdd} from "../../common/EntityAutoCompleteWithAdd";

export function Suppliers(props) {

    const searchSupplier = (query) => {
        return supplierService.getSuppliersByName(query);
    }

    const saveSupplier = (entity) => {
        return supplierService.createSupplier(entity);
    }

    return (
        <EntityAutoCompleteWithAdd
            entity={props.supplier}
            findByQuery={searchSupplier}
            createEntity={saveSupplier}
            onEntityChanged={props.onSupplierChanged}
        />
    )
}