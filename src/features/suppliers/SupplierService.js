import {backendApi} from "../../common/backendApi";

class SupplierService {
    getSuppliersByName(query) {
        return backendApi.get(`/suppliers?name=${query}`)
    }

    createSupplier(newSupplier) {
        return backendApi.post(`/suppliers`,newSupplier)
    }
}

export const supplierService = new SupplierService();