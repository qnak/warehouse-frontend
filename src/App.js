import React, {useState} from 'react';
import './App.css';
import {TabPanel, TabView} from "primereact/tabview";
import {Products} from "./features/products/Products";
import {SupplyOrders} from "./features/supply_orders/SupplyOrders";
import {ProductStock} from "./features/stock/ProductStock";

function App() {
    const [activeIndex, setActiveIndex] = useState(0);

    return (
        <div className="card">
            <div className="flex justify-content-center">
                <div style={{width: '1024px'}}>
                    <h1>Склад "Храни! Принимай!"</h1>
                    <TabView activeIndex={activeIndex}
                             onTabChange={(e) => setActiveIndex(e.index)}>
                        <TabPanel header="Продукты">
                            <Products/>
                        </TabPanel>
                        <TabPanel header="Поставки">
                            <SupplyOrders/>
                        </TabPanel>
                        <TabPanel header="Запас">
                            <ProductStock/>
                        </TabPanel>
                    </TabView>
                </div>
            </div>
        </div>

    );
}

export default App;
