# Frontend Java web-application Склад "Храни! Принимай!"

## Tech Stack

* ReactJS + Redux
* PrimeReact
* Axios

## Запуск

Сборка:

```bash
npm install
```

Старт:

```bash
npm start
```

Или
1. открыть в IntellijIDEA через `File->New->New project from Existing sources`
2. Скомпилировать (build или через maven)
3. Нажать Run напротив scripts.start в package.json 

Конфигурация backend в `src/common/backendApi.js`


## Features

* добавление, редактирование продукта
* добавление поставщиков
* добавление поставок
* просмотр запаса продуктов на дату
 